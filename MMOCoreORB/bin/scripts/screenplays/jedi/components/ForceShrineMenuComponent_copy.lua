local QuestManager = require("managers.quest.quest_manager")
ForceShrineMenuComponent = {}

function ForceShrineMenuComponent:fillObjectMenuResponse(pSceneObject, pMenuResponse, pPlayer)
	local menuResponse = LuaObjectMenuResponse(pMenuResponse)

	if (CreatureObject(pPlayer):hasSkill("force_title_jedi_novice")) then
		menuResponse:addRadialMenuItem(120, 3, "@jedi_trials:meditate") -- Meditate
	end

	if (CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_02")) then
		menuResponse:addRadialMenuItem(121, 3, "@force_rank:recover_jedi_items") -- Recover Jedi Items
		menuResponse:addRadialMenuItem(122, 3, "Grant Jedi Skills")
		if (not CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_03")) then
			menuResponse:addRadialMenuItem(125, 3, "Unlock Knight")
		end
	end
	
	if (not CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_02")) then
		menuResponse:addRadialMenuItem(123, 3, "Unlock Padawan")
	end
	
	if (not CreatureObject(pPlayer):hasSkill("force_title_jedi_novice")) then
		menuResponse:addRadialMenuItem(124, 3, "Unlock Force Sensitive")
	end

end

function ForceShrineMenuComponent:handleObjectMenuSelect(pObject, pPlayer, selectedID)
	if (pPlayer == nil or pObject == nil) then
		return 0
	end

	if (selectedID == 120 and CreatureObject(pPlayer):hasSkill("force_title_jedi_novice")) then
		if (CreatureObject(pPlayer):getPosture() ~= CROUCHED) then
			CreatureObject(pPlayer):sendSystemMessage("@jedi_trials:show_respect") -- Must respect
		else
			self:doMeditate(pObject, pPlayer)
		end
	elseif (selectedID == 121 and CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_02")) then
		self:recoverRobe(pPlayer)
	elseif (selectedID == 122) then
		self:trainJedi(pPlayer)
	elseif (selectedID == 123) then
		ForceShrineMenuComponent:unlockJediPadawan(pPlayer)
	elseif (selectedID == 124) then
		ForceShrineMenuComponent:unlockForceSensitive(pPlayer)
	elseif (selectedID == 125) then
		ForceShrineMenuComponent:unlockKnight(pPlayer)
	end

	return 0
end

function ForceShrineMenuComponent:doMeditate(pObject, pPlayer)
	if (tonumber(readScreenPlayData(pPlayer, "KnightTrials", "completedTrials")) == 1 and not CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_03")) then
		KnightTrials:resetCompletedTrialsToStart(pPlayer)
	end

	if (not CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_02") and CreatureObject(pPlayer):hasScreenPlayState(32, "VillageJediProgression")) then
		local currentTrial = JediTrials:getCurrentTrial(pPlayer)

		if (not JediTrials:isOnPadawanTrials(pPlayer)) then
			PadawanTrials:startPadawanTrials(pObject, pPlayer)
		elseif (currentTrial == 0) then
			PadawanTrials:startNextPadawanTrial(pObject, pPlayer)
		else
			PadawanTrials:showCurrentTrial(pObject, pPlayer)
		end
	elseif (JediTrials:isOnKnightTrials(pPlayer)) then
		local pPlayerShrine = KnightTrials:getTrialShrine(pPlayer)

		if (pPlayerShrine ~= nil and pObject ~= pPlayerShrine) then
			local correctShrineZone = SceneObject(pPlayerShrine):getZoneName()
			if (correctShrineZone ~= SceneObject(pObject):getZoneName()) then
				local messageString = LuaStringIdChatParameter("@jedi_trials:knight_shrine_reminder")
				messageString:setTO(getStringId("@jedi_trials:" .. correctShrineZone))
				CreatureObject(pPlayer):sendSystemMessage(messageString:_getObject())
			else
				CreatureObject(pPlayer):sendSystemMessage("@jedi_trials:knight_shrine_wrong")
			end
			return
		end

		local currentTrial = JediTrials:getCurrentTrial(pPlayer)
		local trialsCompleted = JediTrials:getTrialsCompleted(pPlayer)

		if (currentTrial == 0 and trialsCompleted == 0) then
			local sui = SuiMessageBox.new("KnightTrials", "startNextKnightTrial")
			sui.setTitle("@jedi_trials:knight_trials_title")
			sui.setPrompt("@jedi_trials:knight_trials_start_query")
			sui.setOkButtonText("@jedi_trials:button_yes")
			sui.setCancelButtonText("@jedi_trials:button_no")
			sui.sendTo(pPlayer)
		else
			KnightTrials:showCurrentTrial(pPlayer)
		end
	else
		CreatureObject(pPlayer):sendSystemMessage("@jedi_trials:force_shrine_wisdom_" .. getRandomNumber(1, 15))
	end
end

function ForceShrineMenuComponent:recoverRobe(pPlayer)
	local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")

	if (pInventory == nil) then
		return
	end

	if (SceneObject(pInventory):isContainerFullRecursive()) then
		CreatureObject(pPlayer):sendSystemMessage("@jedi_spam:inventory_full_jedi_robe")
		return
	end

	local robeTemplate
	if (CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_03")) then
		local councilType = JediTrials:getJediCouncil(pPlayer)

		if (councilType == JediTrials.COUNCIL_LIGHT) then
			robeTemplate = "object/tangible/wearables/robe/robe_jedi_light_s01.iff"
		else
			robeTemplate = "object/tangible/wearables/robe/robe_jedi_dark_s01.iff"
		end
	else
		robeTemplate = "object/tangible/wearables/robe/robe_jedi_padawan.iff"
	end

	giveItem(pInventory, robeTemplate, -1)
	CreatureObject(pPlayer):sendSystemMessage("@force_rank:items_recovered")
end

function ForceShrineMenuComponent:unlockKnight(pPlayer)
	if (pPlayer == nil) then
		return
	end
	
	--KnightTrials:sendCouncilChoiceSui(pPlayer)
	
	local sui = SuiListBox.new("ForceShrineMenuComponent", "mainCallBack")
	sui.setTitle("Force Ranking System")
	sui.setPrompt("Select which faction you'd like to serve.")
	sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())
	local playerFaction = CreatureObject(pPlayer):getFaction()
	if (playerFaction == FACTIONIMPERIAL) then
		sui.add("Imperial - Dark Force Ranking", "joinDarkFrs")
	elseif (playerFaction == FACTIONREBEL) then
		sui.add("Rebel - Light Force Ranking", "joinLightFrs")
	else
		sui.add("Imperial - Dark Force Ranking", "joinDarkFrs")
		sui.add("Rebel - Light Force Ranking", "joinLightFrs")
	end
	
	sui.sendTo(pPlayer)
end

function ForceShrineMenuComponent:joinDarkFrs(pPlayer)
	if (pPlayer == nil) then
		return
	end
	
	local pGhost = CreatureObject(pPlayer):getPlayerObject()

	if (pGhost == nil) then
		return
	end
	KnightTrials:doCouncilDecision(pPlayer, JediTrials.COUNCIL_DARK)
	writeScreenPlayData(pPlayer, "KnightTrials", "completedTrials", 1)
	JediTrials:unlockJediKnight(pPlayer)
	CreatureObject(pPlayer):setFaction(FACTIONIMPERIAL)
end

function ForceShrineMenuComponent:joinLightFrs(pPlayer)
	if (pPlayer == nil) then
		return
	end
	
	local pGhost = CreatureObject(pPlayer):getPlayerObject()

	if (pGhost == nil) then
		return
	end
	KnightTrials:doCouncilDecision(pPlayer, JediTrials.COUNCIL_LIGHT)
	writeScreenPlayData(pPlayer, "KnightTrials", "completedTrials", 1)
	JediTrials:unlockJediKnight(pPlayer)
	CreatureObject(pPlayer):setFaction(FACTIONREBEL)
end

function ForceShrineMenuComponent:unlockForceSensitive(pPlayer)
	if (pPlayer == nil) then
		return
	end

	local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")

	if (pInventory == nil) then
		return
	end

	local pGhost = CreatureObject(pPlayer):getPlayerObject()

	if (pGhost == nil) then
		return
	end

	VillageJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, VILLAGE_JEDI_PROGRESSION_GLOWING)

	QuestManager.completeQuest(pPlayer, QuestManager.quests.OLD_MAN_INITIAL)

	giveItem(pInventory, "object/tangible/loot/quest/force_sensitive/force_crystal.iff", -1)

	QuestManager.completeQuest(pPlayer, QuestManager.quests.OLD_MAN_FORCE_CRYSTAL)

	VillageJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, VILLAGE_JEDI_PROGRESSION_HAS_CRYSTAL)

	QuestManager.completeQuest(pPlayer, QuestManager.quests.TWO_MILITARY)
	QuestManager.completeQuest(pPlayer, QuestManager.quests.LOOT_DATAPAD_1)
	QuestManager.completeQuest(pPlayer, QuestManager.quests.GOT_DATAPAD)
	QuestManager.completeQuest(pPlayer, QuestManager.quests.FS_THEATER_CAMP)
	QuestManager.completeQuest(pPlayer, QuestManager.quests.GOT_DATAPAD_2)
	QuestManager.completeQuest(pPlayer, QuestManager.quests.LOOT_DATAPAD_2)

	QuestManager.completeQuest(pPlayer, QuestManager.quests.FS_VILLAGE_ELDER)

	VillageJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, VILLAGE_JEDI_PROGRESSION_HAS_VILLAGE_ACCESS)

	if (not PlayerObject(pGhost):isJedi()) then
		PlayerObject(pGhost):setJediState(1)
	end

	awardSkill(pPlayer, "force_title_jedi_novice")

	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_combat_prowess_ranged_accuracy")
	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_combat_prowess_ranged_speed")
	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_combat_prowess_melee_accuracy")
	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_combat_prowess_melee_speed")
	awardSkill(pPlayer, "force_sensitive_combat_prowess_master")
	
	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_enhanced_reflexes_ranged_defense")
	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_enhanced_reflexes_melee_defense")
	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_enhanced_reflexes_vehicle_control")
	CreatureObject(pPlayer):setScreenPlayState(2, "VillageUnlockScreenPlay:force_sensitive_enhanced_reflexes_survival")
	awardSkill(pPlayer, "force_sensitive_enhanced_reflexes_master")
	
	QuestManager.completeQuest(pPlayer, QuestManager.quests.OLD_MAN_FINAL)
	QuestManager.completeQuest(pPlayer, QuestManager.quests.FS_THEATER_FINAL)
	VillageJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, VILLAGE_JEDI_PROGRESSION_DEFEATED_MELLIACHAE)
	
	writeScreenPlayData(pPlayer, "PadawanTrials", "completedTrials", 1)
	VillageJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, VILLAGE_JEDI_PROGRESSION_COMPLETED_PADAWAN_TRIALS)
	
end

function ForceShrineMenuComponent:unlockJediPadawan(pPlayer)
	if (pPlayer == nil) then
		return
	end

	local pGhost = CreatureObject(pPlayer):getPlayerObject()

	if (pGhost == nil) then
		return
	end
	
	local sui = SuiMessageBox.new("JediTrials", "emptyCallback") -- No callback
	sui.setTitle("@jedi_trials:padawan_trials_title")
	sui.setPrompt("@jedi_trials:padawan_trials_completed")
	sui.sendTo(pPlayer)

	if (not CreatureObject(pPlayer):hasSkill("force_title_jedi_rank_01")) then
		awardSkill(pPlayer, "force_title_jedi_rank_01")
	end

	awardSkill(pPlayer, "force_title_jedi_rank_02")

	CreatureObject(pPlayer):playEffect("clienteffect/trap_electric_01.cef", "")
	CreatureObject(pPlayer):playMusicMessage("sound/music_become_jedi.snd")

	PlayerObject(pGhost):setJediState(2)

	local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")

	if (pInventory == nil or SceneObject(pInventory):isContainerFullRecursive()) then
		CreatureObject(pPlayer):sendSystemMessage("@jedi_spam:inventory_full_jedi_robe")
	else
		local pInventory = CreatureObject(pPlayer):getSlottedObject("inventory")
		local pItem = giveItem(pInventory, "object/tangible/wearables/robe/robe_jedi_padawan.iff", -1)
	end

	sendMail("system", "@jedi_spam:welcome_subject", "@jedi_spam:welcome_body", CreatureObject(pPlayer):getFirstName())
end

function ForceShrineMenuComponent:trainJedi(pPlayer)
	local sui = SuiListBox.new("ForceShrineMenuComponent", "mainCallBack")
	sui.setTitle("Select A Jedi Skill")
	sui.setPrompt("Select from the list of Jedi Skills to train in.")
	sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())
	--Lightsaber
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_novice")) then
		sui.add("Lightsaber Novice", "trainNoviceMaster")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_master")) then
		sui.add("Lightsaber Master", "trainNoviceMaster")
		for o = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_one_hand_0" .. o)) then
				sui.add("Lightsaber One-Handed " .. o, "trainOneHand")
				break
			end
		end
		for t = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_two_hand_0" .. t)) then
				sui.add("Lightsaber Two-Handed " .. t, "trainTwohand")
				break
			end
		end
		for p = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_polearm_0" .. p)) then
				sui.add("Lightsaber Polearm " .. p, "trainPolearm")
				break
			end
		end
		for e = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_technique_0" .. e)) then
				sui.add("Lightsaber Techniques " .. e, "trainTechniques")
				break
			end
		end
	end
	--Powers
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_novice")) then
		sui.add("Force Power Novice", "trainPowersNoviceMaster")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_master")) then
		sui.add("Force Power Master", "trainPowersNoviceMaster")
		for o = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_lightning_0" .. o)) then
				sui.add("Force Lightning " .. o, "trainLightning")
				break
			end
		end
		for t = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_mental_0" .. t)) then
				sui.add("Force Powers Mental " .. t, "trainMental")
				break
			end
		end
		for p = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_debuff_0" .. p)) then
				sui.add("Force Powers Debuff " .. p, "trainDebuff")
				break
			end
		end
		for e = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_push_0" .. e)) then
				sui.add("Force Push " .. e, "trainPush")
				break
			end
		end
	end
	--healer
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_novice")) then
		sui.add("Force Heal Novice", "trainHealNoviceMaster")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_master")) then
		sui.add("Force Heal Master", "trainHealNoviceMaster")
		for o = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_damage_0" .. o)) then
				sui.add("Self Heal " .. o, "trainDamage")
				break
			end
		end
		for t = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_wound_0" .. t)) then
				sui.add("Self Wound " .. t, "trainWound")
				break
			end
		end
		for p = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_other_0" .. p)) then
				sui.add("Other Heal " .. p, "trainOtherHeal")
				break
			end
		end
		for e = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_states_0" .. e)) then
				sui.add("Other States " .. e, "trainOtherStates")
				break
			end
		end
	end
	--enhancements
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_novice")) then
		sui.add("Enhancer Novice", "trainEnhanceNoviceMaster")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_master")) then
		sui.add("Enhancer Master", "trainEnhanceNoviceMaster")
		for o = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_movement_0" .. o)) then
				sui.add("Force Run " .. o, "trainMovement")
				break
			end
		end
		for t = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_protection_0" .. t)) then
				sui.add("Force Protections " .. t, "trainProtection")
				break
			end
		end
		for p = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_resistance_0" .. p)) then
				sui.add("Force Resistances " .. p, "trainResistance")
				break
			end
		end
		for e = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_synergy_0" .. e)) then
				sui.add("Force Regens " .. e, "trainSynergy")
				break
			end
		end
	end
	--defender
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_novice")) then
		sui.add("Defender Novice", "trainDefenderNoviceMaster")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_master")) then
		sui.add("Defender Master", "trainDefenderNoviceMaster")
		for o = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_melee_defense_0" .. o)) then
				sui.add("Melee Defense " .. o, "trainMeleeDef")
				break
			end
		end
		for t = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_ranged_defense_0" .. t)) then
				sui.add("Ranged Defense " .. t, "trainRangedDef")
				break
			end
		end
		for p = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_force_defense_0" .. p)) then
				sui.add("Force Defense " .. p, "trainForceDef")
				break
			end
		end
		for e = 1, 4 do
			if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_preternatural_defense_0" .. e)) then
				sui.add("Jedi State Defense " .. e, "trainStateDef")
				break
			end
		end
	end
	sui.sendTo(pPlayer)
end

function ForceShrineMenuComponent.trainDefenderNoviceMaster(pPlayer)
	if (pPlayer == nil) then
		return
	end
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_novice")) then
		awardSkill(pPlayer, "force_discipline_defender_novice")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_master")) then
		awardSkill(pPlayer, "force_discipline_defender_master")
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainMeleeDef(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_melee_defense_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_defender_melee_defense_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainRangedDef(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_ranged_defense_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_defender_ranged_defense_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainForceDef(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_force_defense_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_defender_force_defense_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainStateDef(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_defender_preternatural_defense_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_defender_preternatural_defense_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainEnhanceNoviceMaster(pPlayer)
	if (pPlayer == nil) then
		return
	end
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_novice")) then
		awardSkill(pPlayer, "force_discipline_enhancements_novice")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_master")) then
		awardSkill(pPlayer, "force_discipline_enhancements_master")
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainMovement(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_movement_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_enhancements_movement_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainProtection(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_protection_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_enhancements_protection_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainResistance(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_resistance_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_enhancements_resistance_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainSynergy(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_enhancements_synergy_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_enhancements_synergy_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainHealNoviceMaster(pPlayer)
	if (pPlayer == nil) then
		return
	end
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_novice")) then
		awardSkill(pPlayer, "force_discipline_healing_novice")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_master")) then
		awardSkill(pPlayer, "force_discipline_healing_master")
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainDamage(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_damage_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_healing_damage_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainWound(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_wound_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_healing_wound_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainOtherHeal(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_other_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_healing_other_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainOtherStates(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_healing_states_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_healing_states_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainPowersNoviceMaster(pPlayer)
	if (pPlayer == nil) then
		return
	end
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_novice")) then
		awardSkill(pPlayer, "force_discipline_powers_novice")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_master")) then
		awardSkill(pPlayer, "force_discipline_powers_master")
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainLightning(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_lightning_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_powers_lightning_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainMental(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_mental_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_powers_mental_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainDebuff(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_debuff_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_powers_debuff_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainPush(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_powers_push_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_powers_push_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainNoviceMaster(pPlayer)
	if (pPlayer == nil) then
		return
	end
	if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_novice")) then
		awardSkill(pPlayer, "force_discipline_light_saber_novice")
	elseif (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_master")) then
		awardSkill(pPlayer, "force_discipline_light_saber_master")
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainOneHand(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_one_hand_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_light_saber_one_hand_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainTwohand(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_two_hand_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_light_saber_two_hand_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainPolearm(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_polearm_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_light_saber_polearm_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent.trainTechniques(pPlayer)
	if (pPlayer == nil) then
		return
	end
	for u = 1, 4 do
    	if (not CreatureObject(pPlayer):hasSkill("force_discipline_light_saber_technique_0" .. u)) then
			awardSkill(pPlayer,"force_discipline_light_saber_technique_0" .. u)
        	break
    	end
	end
	ForceShrineMenuComponent:trainJedi(pPlayer)
end

function ForceShrineMenuComponent:mainCallBack(pPlayer, pSui, eventIndex, args)
	local cancelPressed = (eventIndex == 1)

	if (cancelPressed or args == nil or tonumber(args) < 0) then
		return
	end

	local pPageData = LuaSuiBoxPage(pSui):getSuiPageData()

	if (pPageData == nil) then
		return
	end

	local suiPageData = LuaSuiPageData(pPageData)
	local menuOption = suiPageData:getStoredData(tostring(args))

	if (string.find(menuOption, "%d")) then
		menuOption = string.gsub(menuOption, "")
	end

	if (self[menuOption] == nil) then
		printLuaError("Tried to execute invalid function " .. menuOption .. " in customSui")
		return
	end

	self[menuOption](pPlayer)
end
