--Conversations
includeFile("../custom_scripts/mobile/conversations/serverobjects.lua")

-- ## Brawler's Guild Includes
--Quest givers
includeFile("../custom_scripts/mobile/brawlersGuild/rafa.lua")
includeFile("../custom_scripts/mobile/brawlersGuild/trev.lua")
includeFile("../custom_scripts/mobile/brawlersGuild/jamgi.lua")

--Quest targets
includeFile("../custom_scripts/mobile/brawlersGuild/basement_rat.lua")
includeFile("../custom_scripts/mobile/brawlersGuild/lesk.lua")
includeFile("../custom_scripts/mobile/brawlersGuild/lesk_thug.lua")
includeFile("../custom_scripts/mobile/brawlersGuild/jaxun.lua")
includeFile("../custom_scripts/mobile/brawlersGuild/jaxun_easy.lua")

-- ## End Brawler's Guild Includes

--Misc folder
includeFile("../custom_scripts/mobile/misc/tutorial_droid.lua")

--Restoration
includeFile("../custom_scripts/mobile/resto/bonKar.lua")
includeFile("../custom_scripts/mobile/resto/kruk.lua")
includeFile("../custom_scripts/mobile/resto/juri.lua")
includeFile("../custom_scripts/mobile/resto/juri_thug.lua")
includeFile("../custom_scripts/mobile/resto/krukAssistant.lua")