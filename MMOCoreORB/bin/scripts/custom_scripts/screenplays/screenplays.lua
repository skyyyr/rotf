--Quest handlers
includeFile("../custom_scripts/screenplays/brawlersGuild/brawlerGuildHandler.lua")

--Admin Tools
includeFile("../custom_scripts/screenplays/rotfTools/adminTools.lua")
includeFile("../custom_scripts/screenplays/rotfTools/spawnArea.lua")
includeFile("../custom_scripts/screenplays/rotfTools/questTool.lua")
includeFile("../custom_scripts/screenplays/rotfTools/helperScript.lua")

--Restoration
includeFile("../custom_scripts/screenplays/resto/vipBunker.lua")
includeFile("../custom_scripts/screenplays/resto/swoopGangHandler.lua")