local ObjectManager = require("managers.object.object_manager")

swoopGangScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "swoopGangScreenPlay",

}

registerScreenPlay("swoopGangScreenPlay", true)

function swoopGangScreenPlay:start()
	if (isZoneEnabled("talus") and isZoneEnabled("tatooine") and isZoneEnabled("endor")) then
		self:spawnMobiles()
		self:spawnBikes()
	end
end

function swoopGangScreenPlay:spawnMobiles()
	--Talus Nashal Kruk Office
	local office = 6255474
	--Mos Espa Cantina main room	
	local cellMain = 1256062
	--Mos Espa Cantina big room
	local cellBig = 1256058
	--Wayfar cantina
	local wayfar = 1134560
	--Mos Eisley Cantina
	local eisley = 1082885
	
	local planet = "talus"
	--Talus spawns
	local pKruk = spawnMobile(planet, "kruk", 0, 35.6, 1.3, -2.1, -133, office)
	self:setMoodString(pKruk, "npc_sitting_chair")
	
	local pNpc = spawnMobile(planet, "krukAssistant", 0, 29, 1.3, -7.1, 10, office)
	self:setMoodString(pNpc, "npc_sitting_chair")
	writeData(SceneObject(pKruk):getObjectID() .. ":swoopGang:synthia", SceneObject(pNpc):getObjectID())
	
	local planet = "tatooine"
	--Tatooine spawns
	--Mos Espa spawns
	local pJuri = spawnMobile(planet, "juri", 0, 3.2, -0.9, 22.6, -179, cellMain)
	self:setMoodString(pJuri, "npc_sitting_chair")
	writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChat", 1)
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, 7.5, -0.9, 21.3, 145, cellMain)
	self:setMoodString(pNpc, "npc_sitting_chair")
	CreatureObject(pNpc):setCustomObjectName("Fihl (Blood Moon Gang Member)")
	writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChatFihl", SceneObject(pNpc):getObjectID())
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, 10, -0.9, 21.6, -141, cellMain)
	self:setMoodString(pNpc, "npc_sitting_chair")
	CreatureObject(pNpc):setCustomObjectName("Lyhl (Blood Moon Gang Member)")
	writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChatLyhl", SceneObject(pNpc):getObjectID())
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, 9.7, -0.9, 18, -14, cellMain)
	self:setMoodString(pNpc, "npc_sitting_chair")
	CreatureObject(pNpc):setCustomObjectName("Tomi (Blood Moon Gang Member)")
	writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChatTomi", SceneObject(pNpc):getObjectID())
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, 7.1, -0.9, 18.7, 46, cellMain)
	self:setMoodString(pNpc, "npc_sitting_chair")
	CreatureObject(pNpc):setCustomObjectName("Chucki (Blood Moon Gang Member)")
	writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChatChucki", SceneObject(pNpc):getObjectID())
	
	local pTender = spawnMobile(planet, "juri_thug", 0, 7.6, -0.9, 2.4, 40, cellBig)
	self:setMoodString(pTender, "conversation")
	CreatureObject(pTender):setCustomObjectName("Bart'ander Purs'demstron")
	
	--Wayfar spawns
	local pWayfar = spawnMobile(planet, "juri_thug", 0, 8.5, -0.9, 1, 85, wayfar)
	self:setMoodString(pWayfar, "conversation")
	CreatureObject(pWayfar):setCustomObjectName("Spyk (Blood Moon Gang Member)")
	writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChat", 1)
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, 10.8, -0.9, 1, -76, wayfar)
	self:setMoodString(pNpc, "conversation")
	CreatureObject(pNpc):setCustomObjectName("Didi (Blood Moon Gang Member)")
	writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChatDidi", SceneObject(pNpc):getObjectID())
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, 10.5, -0.9, 3.2, -143, wayfar)
	self:setMoodString(pNpc, "conversation")
	CreatureObject(pNpc):setCustomObjectName("Stu (Blood Moon Gang Member)")
	writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChatStu", SceneObject(pNpc):getObjectID())
	
	--Mos Eisley Spawns
	local pEisley = spawnMobile(planet, "juri_thug", 0, -25.4, -0.9, -1, -29, eisley)
	self:setMoodString(pEisley, "conversation")
	CreatureObject(pEisley):setCustomObjectName("Anjel'ika (Blood Moon Gang Member)")
	writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChat", 1)
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, -27.9, -0.9, 2.2, 128, eisley)
	self:setMoodString(pNpc, "conversation")
	CreatureObject(pNpc):setCustomObjectName("Druu (Blood Moon Gang Member)")
	writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChatDru", SceneObject(pNpc):getObjectID())
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, -24, -0.9, 1.8, -136, eisley)
	self:setMoodString(pNpc, "conversation")
	CreatureObject(pNpc):setCustomObjectName("Char'lett (Blood Moon Gang Member)")
	writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChatChar", SceneObject(pNpc):getObjectID())
	
	local pNpc = spawnMobile(planet, "juri_thug", 0, -22.7, -0.9, 0.9, -108, eisley)
	self:setMoodString(pNpc, "conversation")
	CreatureObject(pNpc):setCustomObjectName("Betty (Blood Moon Gang Member)")
	writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChatBetty", SceneObject(pNpc):getObjectID())
	
	
	
	
	createEvent(10000, "swoopGangScreenPlay", "rugratsChatter", pJuri, "")
	createEvent(10000, "swoopGangScreenPlay", "wayfarChatter", pWayfar, "")
	createEvent(10000, "swoopGangScreenPlay", "eisleyChatter", pEisley, "")
end

function swoopGangScreenPlay:spawnBikes()
	--Spawn Bikes object/mobile/vehicle/speederbike.iff
	local planet = "tatooine"
	--Mos Eisley Speeders
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", 3457, 5, -4861, 127, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", 3455, 5, -4861, 127, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", 3453, 5, -4861, 127, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", 3451, 5, -4861, 127, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	
	--Wayfar Speeders
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", -5136, 75, -6552, 179, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", -5138, 75, -6552, 179, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", -5140, 75, -6552, 179, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	
	--Mos Espa
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike_swoop.iff", -2972, 5, 2184, -20, 0)
	CreatureObject(pBike):setCustomObjectName("Juri's Swoop (Blood Moon Gang)")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", -2970, 5, 2184, -20, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", -2968, 5, 2184, -20, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", -2966, 5, 2184, -20, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	local pBike = spawnSceneObject(planet, "object/mobile/vehicle/speederbike.iff", -2964, 5, 2184, -20, 0)
	CreatureObject(pBike):setCustomObjectName("Blood Moon Speederbike")
	
	--Add swoops to talus and endor
end

function swoopGangScreenPlay:rugratsChatter(pJuri)
	local juriID = SceneObject(pJuri):getObjectID()
	local fihlID = readData(juriID .. ":swoopGang:spatialChatFihl")
	local lyhlID = readData(juriID .. ":swoopGang:spatialChatLyhl")
	local tomiID = readData(juriID .. ":swoopGang:spatialChatTomi")
	local chuckiID = readData(juriID .. ":swoopGang:spatialChatChucki")
	local chatNumber = readData(juriID .. ":swoopGang:spatialChat")
	local pFihl = getSceneObject(fihlID)
	local pLyhl = getSceneObject(lyhlID)
	local pTomi = getSceneObject(tomiID)
	local pChucki = getSceneObject(chuckiID)
	
	if (chatNumber == 1) then
		spatialChat(pFihl, "Why does Anjelika gots to be so bossy??")
		createEvent(8000, "swoopGangScreenPlay", "rugratsChatter", pJuri, "")
		writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChat", 2)
	elseif (chatNumber == 2) then
		spatialChat(pLyhl, "Yeah! She's a meany beany face!")
		createEvent(8000, "swoopGangScreenPlay", "rugratsChatter", pJuri, "")
		writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChat", 3)
	elseif (chatNumber == 3) then
		spatialChat(pChucki, "Remember when she said Spyke was my brother?")
		createEvent(8000, "swoopGangScreenPlay", "rugratsChatter", pJuri, "")
		writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChat", 4)
	elseif (chatNumber == 4) then
		spatialChat(pTomi, "Haha! That was great though wasn't it?")
		createEvent(8000, "swoopGangScreenPlay", "rugratsChatter", pJuri, "")
		writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChat", 5)
	else
		spatialChat(pJuri, "You four rats will be turned into rugs if you keep it up..")
		createEvent(5 * 60 * 1000, "swoopGangScreenPlay", "rugratsChatter", pJuri, "")
		writeData(SceneObject(pJuri):getObjectID() .. ":swoopGang:spatialChat", 1)
	end
end

function swoopGangScreenPlay:activateQuest(pPlayer)
	local playerID = SceneObject(pPlayer):getObjectID()
	local synthiaID = readData(playerID .. ":swoopGang:synthia")
	local pSynthia = getSceneObject(synthiaID)
	local subject = "Covert Op Mission"
	local body = "##################### You've been given clearance to read this message ##############\n\n\n" ..
				 "Covert Operation BMG-L33T\n\n\n" ..
				 "The 'Blood Moon' swoop gang was just a local Tatooine gang that has now become a multi-planet operation.\n" ..
				 "It is believed that they became big by making a deal with Jabba the Hutt.\n" ..
				 "Previous opertive (Code Name: Stu-art) has gone missing after delivering this report.\n" ..
				 "By bribing local scum we have found that the gang recruits out of tatooine, especially out of the cantinas.\n" ..
				 "We haven't been able to get one of our agents in the gang yet. We need to know who they plan on hitting next.\n" ..
				 "They enjoy stealing swoops, killing locals, and robbing local stores on Tatooine.\n" ..
				 "##################### END REPORT #################################"
	
	spatialChat(pSynthia, "Sir, I've sent " .. SceneObject(pPlayer):getCustomObjectName() .. " the holo-mail with the mission details.")
	helperScript:mailTarget(pPlayer, SceneObject(pSynthia):getCustomObjectName(), subject, body)
end

function swoopGangScreenPlay:wayfarChatter(pWayfar)
	local wayfarID = SceneObject(pWayfar):getObjectID()
	local didiID = readData(wayfarID .. ":swoopGang:spatialChatDidi")
	local stuID = readData(wayfarID .. ":swoopGang:spatialChatStu")
	local chatNumber = readData(wayfarID .. ":swoopGang:spatialChat")
	local pDidi = getSceneObject(didiID)
	local pStu = getSceneObject(stuID)
	
	if (chatNumber == 1) then
		spatialChat(pStu, "Here you go, boy. Burnt to a crisp, just as you like them. *gives burnt bantha meat to Spyk* ")
		createEvent(8000, "swoopGangScreenPlay", "wayfarChatter", pWayfar, "")
		writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChat", 2)
	elseif (chatNumber == 2) then
		spatialChat(pWayfar, "Yeah yeah yeah! Yumm!")
		createEvent(8000, "swoopGangScreenPlay", "wayfarChatter", pWayfar, "")
		writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChat", 3)
	elseif (chatNumber == 3) then
		spatialChat(pDidi, "Stu! Don't over feed him!")
		createEvent(8000, "swoopGangScreenPlay", "wayfarChatter", pWayfar, "")
		writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChat", 4)
	elseif (chatNumber == 4) then
		spatialChat(pStu, "Haha! Over feed Spyk? Is that even possible?")
		createEvent(8000, "swoopGangScreenPlay", "wayfarChatter", pWayfar, "")
		writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChat", 5)
	else
		spatialChat(pDidi, "I guess not...")
		createEvent(5 * 60 * 1000, "swoopGangScreenPlay", "wayfarChatter", pWayfar, "")
		writeData(SceneObject(pWayfar):getObjectID() .. ":swoopGang:spatialChat", 1)
	end
end

function swoopGangScreenPlay:eisleyChatter(pEisley)
	local angelID = SceneObject(pEisley):getObjectID()
	local druID = readData(angelID .. ":swoopGang:spatialChatDru")
	local charID = readData(angelID .. ":swoopGang:spatialChatChar")
	local bettyID = readData(angelID .. ":swoopGang:spatialChatBetty")
	local chatNumber = readData(angelID .. ":swoopGang:spatialChat")
	local pDru = getSceneObject(druID)
	local pChar = getSceneObject(charID)
	local pBetty = getSceneObject(bettyID)
	
	if (chatNumber == 1) then
		spatialChat(pEisley, "I finally have loyal suspects! But they are just dumb babies!")
		createEvent(8000, "swoopGangScreenPlay", "eisleyChatter", pEisley, "")
		writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChat", 2)
	elseif (chatNumber == 2) then
		spatialChat(pDru, "Hey! I'm not a baby!")
		createEvent(8000, "swoopGangScreenPlay", "eisleyChatter", pEisley, "")
		writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChat", 3)
	elseif (chatNumber == 3) then
		spatialChat(pChar, "What's a loyal suspect?")
		createEvent(8000, "swoopGangScreenPlay", "eisleyChatter", pEisley, "")
		writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChat", 4)
	elseif (chatNumber == 4) then
		spatialChat(pBetty, "Well you both are dumb!")
		createEvent(8000, "swoopGangScreenPlay", "eisleyChatter", pEisley, "")
		writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChat", 5)
	else
		spatialChat(pEisley, "Just shut up! Listen to what I have to say from here on out!")
		createEvent(5 * 60 * 1000, "swoopGangScreenPlay", "eisleyChatter", pEisley, "")
		writeData(SceneObject(pEisley):getObjectID() .. ":swoopGang:spatialChat", 1)
	end
end


kruk_handler = conv_handler:new {}
--Need to add "complete" check
function kruk_handler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)
	local questState = CreatureObject(pPlayer):getScreenPlayState("swoopQuest")
	if (questState == 1) then
		return convoTemplate:getScreen("already")
	else
		return convoTemplate:getScreen("first")
	end
end

function kruk_handler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()

	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)
	
	local npcID = SceneObject(pNpc):getObjectID()
	local synthiaID = readData(npcID .. ":swoopGang:synthia")
	local pSynthia = getSceneObject(synthiaID)
	
	if (screenID == "accept_mission") then
		writeData(SceneObject(pPlayer):getObjectID() .. ":swoopGang:synthia", SceneObject(pSynthia):getObjectID())
		createEvent(5000, "swoopGangScreenPlay", "activateQuest", pPlayer, "")
		CreatureObject(pPlayer):setScreenPlayState(1, "swoopQuest")
	end
	return pConvScreen
end